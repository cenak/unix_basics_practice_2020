#include <stdio.h>
#include <stdlib.h>

/*
* - Печатает на экран начало сегмента данных ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
* - Печатает на экран начало сегмента стека +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
* - Печатает на экран начало сегмента кучи ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
* - Печатает на экран начало сегмента кода (достаточно адреса main) +++++++++++++++++++++++++++++++++++++++++++++++++
* - Печатает на экран адрес сегмента разделяемой библиотеки glibc (достаточно адрес printf) +++++++++++++++++++++++++
* - Определяет в каком направлении происходит рост стека (вниз или вверх по адресам) и печатает это на экран ++++++++
*/

const int a = 5;

int main(int argc, char *argv[])
{
	int v = 5;
	void* ptr;
	void* anotherPtr;
	
    printf("Code is at %p \n", (void *)main);
	printf("Data is at %p \n", (void *)&a);
    printf("Stack is at %p \n", (void *)&v);
    printf("Heap is at %p \n", ptr = malloc(8));
	printf("GLIBC is at %p \n", (void *)printf);
	
	anotherPtr = malloc(8);
	
	if(anotherPtr > ptr)
		printf("Stack is growing up\n");
	else
		printf("Stack is growing down\n");
	
	free(ptr);
	free(anotherPtr);
	
    return 0;
}