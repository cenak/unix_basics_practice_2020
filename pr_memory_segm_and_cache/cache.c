#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <windows.h>

#define MB 1000000
#define LENGTH 100

/*
- Функция записи массива 1 Мб по строкам ++++++++++++++++++++++++++++++++++++++++++
- Функция записи массива 1 Мб по столбцам +++++++++++++++++++++++++++++++++++++++++
- Для каждой функции замерить время выполнения при помощи clock_gettime +++++++++++
*/

// Auxiliary
LARGE_INTEGER getFILETIMEoffset();
int clock_gettime(int X, struct timeval *tv);

// line array filling
int FillByLine(int *array[LENGTH][LENGTH])
{
    for(int i = 0; i < LENGTH; i++)
	{
        for(int j = 0; j < LENGTH; j++)
		{
            array[i][j] = (int*) malloc(MB);
        }
    }
	
    return 0;
}

// column array filling
int FillByColumn(int *array[LENGTH][LENGTH])
{
    for(int i = 0; i < LENGTH; i++)
	{
        for(int j = 0; j < LENGTH; j++)
		{
            array[j][i] = (int*) malloc(MB);
        }
    }
	
    return 0;
}

int main(int argc, char *argv[])
{
	int *array1[LENGTH][LENGTH];
	int *array2[LENGTH][LENGTH];	
	
	struct timeval startTime;
	struct timeval endTime;
	
	clock_gettime(0, &startTime);
	
	FillByLine(array1);
	
	clock_gettime(0, &endTime);
	int array1Time = endTime.tv_usec - startTime.tv_usec;
	
	//-------------------------------------------------
	
	clock_gettime(0, &startTime);
	
	FillByColumn(array2);
	
	clock_gettime(0, &endTime);
	int array2Time = endTime.tv_usec - startTime.tv_usec;
	
	printf("Filling by line = %d us (%f s)\n", array1Time, (float)array1Time/1000000);
	printf("Filling by column = %d us (%f s)\n", array2Time, (float)array2Time/1000000);
	
	return 0;
}

LARGE_INTEGER getFILETIMEoffset()
{
    SYSTEMTIME s;
    FILETIME f;
    LARGE_INTEGER t;

    s.wYear = 1970;
    s.wMonth = 1;
    s.wDay = 1;
    s.wHour = 0;
    s.wMinute = 0;
    s.wSecond = 0;
    s.wMilliseconds = 0;
    SystemTimeToFileTime(&s, &f);
    t.QuadPart = f.dwHighDateTime;
    t.QuadPart <<= 32;
    t.QuadPart |= f.dwLowDateTime;
    return (t);
}

int clock_gettime(int X, struct timeval *tv)
{
    LARGE_INTEGER t;
    FILETIME f;
    double microseconds;
    static LARGE_INTEGER offset;
    static double frequencyToMicroseconds;
    static int initialized = 0;
    static BOOL usePerformanceCounter = 0;

    if (!initialized) {
        LARGE_INTEGER performanceFrequency;
        initialized = 1;
        usePerformanceCounter = QueryPerformanceFrequency(&performanceFrequency);
        if (usePerformanceCounter) {
            QueryPerformanceCounter(&offset);
            frequencyToMicroseconds = (double)performanceFrequency.QuadPart / 1000000.;
        } else {
            offset = getFILETIMEoffset();
            frequencyToMicroseconds = 10.;
        }
    }
    if (usePerformanceCounter) QueryPerformanceCounter(&t);
    else {
        GetSystemTimeAsFileTime(&f);
        t.QuadPart = f.dwHighDateTime;
        t.QuadPart <<= 32;
        t.QuadPart |= f.dwLowDateTime;
    }

    t.QuadPart -= offset.QuadPart;
    microseconds = (double)t.QuadPart / frequencyToMicroseconds;
    t.QuadPart = microseconds;
    tv->tv_sec = t.QuadPart / 1000000;
    tv->tv_usec = t.QuadPart % 1000000;
    return (0);
}