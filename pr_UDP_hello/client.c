#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <winsock2.h>
#include <winsock.h>

#pragma comment(lib,"ws2_32.lib") //Winsock Library

#define SERVER "127.0.0.1"	//ip address of udp server
#define BUFLEN 6	//Length of buffer
#define PORT 8888	//The port on which to listen for incoming data

int main(int argc, char *argv[])
{
	struct sockaddr_in si_other;
	int s, slen=sizeof(si_other);
	char buf[BUFLEN];
	char message[BUFLEN];
	WSADATA wsa;

	//Initialise winsock
	printf("\nInitialising Winsock... ");
	if(WSAStartup(MAKEWORD(2,2),&wsa) != 0)
	{
		printf("Failed. Error Code : %d",WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	printf("Initialised.\n");
	
	//create socket
	if((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
	{
		printf("socket() failed with error code : %d" , WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	
	//setup address structure
	memset((char *) &si_other, 0, sizeof(si_other));
	si_other.sin_family = AF_INET;
	si_other.sin_port = htons(PORT);
	si_other.sin_addr.S_un.S_addr = inet_addr(SERVER);
	
	int cmd = 0;
	printf("Input number:\n 1 - start\n 2 - stop\n");
	scanf("%d", &cmd);
	
	switch(cmd)
	{
		case 1:
			message[0] = 's';
			message[1] = 't';
			message[2] = 'a';
			message[3] = 'r';
			message[4] = 't';
			message[5] = '\0';
			break;
		case 2:
			message[0] = 's';
			message[1] = 't';
			message[2] = 'o';
			message[3] = 'p';
			message[4] = '\0';
			break;
		default:
			printf("Error. Wrong number\n");
			return -1;
			break;
	}
	
	//send the message
	if(sendto(s, message, strlen(message) , 0 , (struct sockaddr *) &si_other, slen) == SOCKET_ERROR)
	{
		printf("sendto() failed with error code : %d" , WSAGetLastError());
		exit(EXIT_FAILURE);
	}
		

	//clear the buffer by filling null, it might have previously received data
	memset(buf,'\0', BUFLEN);
	
	/*
	//try to receive some data, this is a blocking call
	if(recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen) == SOCKET_ERROR)
	{
		printf("recvfrom() failed with error code : %d" , WSAGetLastError());
		exit(EXIT_FAILURE);
	}*/
	
	closesocket(s);
	WSACleanup();
	
	return 0;
}