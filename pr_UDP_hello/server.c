#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <winsock2.h>
#include <winsock.h>

#pragma comment(lib,"ws2_32.lib") //Winsock Library

#define BUFLEN 6	//Max length of buffer
#define PORT 8888	//The port on which to listen for incoming data

#include <tchar.h>
#include <windows.h>

DWORD WINAPI PrintHello(void* data) {
	while(1)
		printf("Hello \n");
	
	return 0;
}

int main(int argc, char *argv[])
{
	SOCKET s;
	struct sockaddr_in server, si_other;
	int slen , recv_len;
	char buf[BUFLEN];
	WSADATA wsa;
	

	slen = sizeof(si_other)	;
	
	//Initialise winsock
	printf("\nInitialising Winsock... ");
	if (WSAStartup(MAKEWORD(2,2),&wsa) != 0)
	{
		printf("Failed. Error Code : %d",WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	printf("Initialised.\n");
	
	//Create a socket
	if((s = socket(AF_INET , SOCK_DGRAM , 0 )) == INVALID_SOCKET)
	{
		printf("Could not create socket : %d" , WSAGetLastError());
	}
	printf("Socket created.\n");
	
	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons( PORT );
	
	//Bind
	if( bind(s ,(struct sockaddr *)&server , sizeof(server)) == SOCKET_ERROR)
	{
		printf("Bind failed with error code : %d" , WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	puts("Bind done");
	
	for (;;) /* Run forever */
	{
		printf("Waiting for data...");
		fflush(stdout);
		
		//clear the buffer by filling null, it might have previously received data
		memset(buf,'\0', BUFLEN);
		
		/*
		while( (recv_len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) != 0)
		{
			printf("kek\n");
			
			//print details of the client/peer and the data received
			printf("Received packet from %s:%d\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port));
			printf("Data: %s\n" , buf);
			
			//Start
			if(buf[2] == 'a')
			{
				//thread = CreateThread(NULL, 0, PrintHello, NULL, 0, NULL);
			}
			//Stop
			else if(buf[2] == 'o')
			{
				printf("kek = %d", TerminateThread(thread, 0));
				CloseHandle(thread);
			}
		}
		*/
		
		//try to receive some data, this is a blocking call
		if ((recv_len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) == SOCKET_ERROR)
		{
			printf("recvfrom() failed with error code : %d" , WSAGetLastError());
			exit(EXIT_FAILURE);
		}
		
		//print details of the client/peer and the data received
		printf("Received packet from %s:%d\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port));
		printf("Data: %s\n" , buf);
		
		HANDLE thread = NULL;
		
		//Start
		if(buf[2] == 'a')
		{
			thread = CreateThread(NULL, 0, PrintHello, NULL, 0, NULL);
		}
		//Stop
		else if(buf[2] == 'o')
		{
			TerminateThread(thread, 0);
			CloseHandle(thread);
		}

	}
	
	closesocket(s);
	WSACleanup();
}