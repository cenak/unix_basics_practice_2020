#include <linux/mm.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Kachalin V.S.");
MODULE_DESCRIPTION("This is kernel module practice");

int init_module(void)‏
{
	printk(KERN_INFO "Memory allocation\n");
	int* mem = (int*)kmalloc(1000000, GFP_KERNEL);

	return 0;
}

void cleanup_module(void)‏ {
	printk(KERN_INFO "freeing up memory\n");
	kfree(mem);
}