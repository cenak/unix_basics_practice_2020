#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <time.h>

#define SHAREDSEM "sharedSem"
#define SHAREDMEM "sharedMem"

int main(int argc, char *argv[])
{
	char *ptr;
	int sd;
	sem_t *sm;
	
	errno = 0;

	sem_unlink(SHAREDSEM);
	if((sm = sem_open(SHAREDSEM, O_CREAT|O_EXCL, 0777, 1)) == SEM_FAILED)
		printf("Error: sem_open. %s\n",strerror(errno));

	sd = shm_open(SHAREDMEM, O_RDWR | O_CREAT, 0666);
	ftruncate(sd, 1000);
	
	ptr = mmap(0,1000,PROT_READ | PROT_WRITE,MAP_SHARED,sd,0);
	*ptr = 0;

	close(sd);

	int i =0;
	
	//Without timer
	/*
	while(1)
	{
		sem_wait(sm);
		sprintf(ptr, "%d\n",i);
		sem_post(sm);
		if(i < INT_MAX - 1)
			i++;
	}*/

	//With timer
	time_t start, end;
	int t = 1;
	start = clock();
	while(1)
	{
		if(t >= 1)
		{
			start=clock();
			sem_wait(sm);
			sprintf(ptr, "%d\n",i);
			sem_post(sm);
			if(i < INT_MAX - 1)
				i++;
		}

		end=clock();
		t=(end-start)/CLOCKS_PER_SEC;
	}

	shm_unlink(SHAREDMEM);

	return 0;
}
