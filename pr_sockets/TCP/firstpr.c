#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <winsock2.h>
#include <winsock.h>

#pragma comment(lib,"ws2_32.lib") //Winsock Library

#define SERVER "127.0.0.1"	//ip address of udp server
#define BUFLEN 8182	//Max length of buffer
#define PORT 8888	//The port on which to listen for incoming data

#define NUMOFPACKETS 256
#define MAXNUMOFHISTOGRAMELEMENTS 200 //10, 20, 30, ... (elements 0..9, 10..19, ...)

LARGE_INTEGER getFILETIMEoffset();

int clock_gettime(int X, struct timeval *tv);

int main(int argc, char *argv[])
{
	WSADATA wsa;
	SOCKET s;
	struct sockaddr_in server;
	char *message , server_reply[2000];
	int recv_size;

	printf("\nTCP server\n");

	printf("\nInitialising Winsock... ");
	if(WSAStartup(MAKEWORD(2,2),&wsa) != 0)
	{
		printf("Failed. Error Code : %d",WSAGetLastError());
		return 1;
	}
	
	printf("Initialised.\n");
	
	//Create a socket
	if((s = socket(AF_INET , SOCK_STREAM , 0 )) == INVALID_SOCKET)
	{
		printf("socket() failed with error code :  %d" , WSAGetLastError());
	}

	printf("Socket created.\n");
	
	
	server.sin_addr.s_addr = inet_addr(SERVER);
	server.sin_family = AF_INET;
	server.sin_port = htons(PORT);

	//Connect to remote server
	if(connect(s , (struct sockaddr *)&server , sizeof(server)) < 0)
	{
		printf("connect error");
		return 1;
	}
	
	printf("Connected\n");
	
	
	int sizeOfPacket = 0;
	
	if(argc > 1)
		sizeOfPacket = atoi(argv[1]);
	else
		sizeOfPacket = 16;
	
	printf("size of packet = %d\n",sizeOfPacket);
	
	char* packet = (char*)malloc(sizeOfPacket);
	
	packet[0] = 'h';
	packet[1] = 'e';
	packet[2] = 'l';
	packet[3] = 'l';
	packet[4] = 'o';

	int timeOfPacketsAdventure[NUMOFPACKETS] = {0};
	
	for(int i = 0; i < NUMOFPACKETS; i++)
	{
		
		if(send(s , packet , sizeOfPacket , 0) < 0)
		{
			printf("Sending failed\n");
			return 1;
		}
		
		struct timeval startTime;
		clock_gettime(0, &startTime);
		
		//Receive a reply from the server
		if((recv_size = recv(s , server_reply , 2000 , 0)) == SOCKET_ERROR)
		{
			printf("recv failed\n");
		}
		
		struct timeval endTime;
		clock_gettime(0, &endTime);

		timeOfPacketsAdventure[i] = endTime.tv_usec - startTime.tv_usec;

		printf("%d us\n",timeOfPacketsAdventure[i]);
	}
	
	closesocket(s);
	WSACleanup();
	
	int averageTime = 0;
	int minTime = timeOfPacketsAdventure[0];
	int maxTime = timeOfPacketsAdventure[0];
	
	for(int i = 0; i < NUMOFPACKETS; i++)
	{
		if(minTime > timeOfPacketsAdventure[i])
			minTime = timeOfPacketsAdventure[i];
		
		if(maxTime < timeOfPacketsAdventure[i])
			maxTime = timeOfPacketsAdventure[i];
		
		averageTime += timeOfPacketsAdventure[i];
	}
	
	averageTime /= NUMOFPACKETS;
	
	int numbersForHistogram[MAXNUMOFHISTOGRAMELEMENTS / 10] = {0};
	int packetsInHistogram = 0;
	
	for(int i = 0; i < MAXNUMOFHISTOGRAMELEMENTS / 10; i++)
	{
		for(int j = 0; j < NUMOFPACKETS; j++)
		{
			if(i * 10 <= timeOfPacketsAdventure[j] && timeOfPacketsAdventure[j] < i * 10 + 10)
			{
				numbersForHistogram[i]++;
				packetsInHistogram++;
			}
		}
	}
	
	printf("\nData for histogram:\n");
	
	for(int i = 0; i < MAXNUMOFHISTOGRAMELEMENTS / 10; i++)
		printf("%d..%d = %d\n", i * 10, i * 10 + 9, numbersForHistogram[i]);
	
	printf("\nmax time = %d; min time = %d; average time = %d\n", maxTime, minTime, averageTime);
	printf("\ntotal = %d packets; packets in histogram: %d; missed packets: %d\n", NUMOFPACKETS, packetsInHistogram, NUMOFPACKETS - packetsInHistogram);
	
	return 0;
}

LARGE_INTEGER getFILETIMEoffset()
{
    SYSTEMTIME s;
    FILETIME f;
    LARGE_INTEGER t;

    s.wYear = 1970;
    s.wMonth = 1;
    s.wDay = 1;
    s.wHour = 0;
    s.wMinute = 0;
    s.wSecond = 0;
    s.wMilliseconds = 0;
    SystemTimeToFileTime(&s, &f);
    t.QuadPart = f.dwHighDateTime;
    t.QuadPart <<= 32;
    t.QuadPart |= f.dwLowDateTime;
    return (t);
}

int clock_gettime(int X, struct timeval *tv)
{
    LARGE_INTEGER t;
    FILETIME f;
    double microseconds;
    static LARGE_INTEGER offset;
    static double frequencyToMicroseconds;
    static int initialized = 0;
    static BOOL usePerformanceCounter = 0;

    if (!initialized) {
        LARGE_INTEGER performanceFrequency;
        initialized = 1;
        usePerformanceCounter = QueryPerformanceFrequency(&performanceFrequency);
        if (usePerformanceCounter) {
            QueryPerformanceCounter(&offset);
            frequencyToMicroseconds = (double)performanceFrequency.QuadPart / 1000000.;
        } else {
            offset = getFILETIMEoffset();
            frequencyToMicroseconds = 10.;
        }
    }
    if (usePerformanceCounter) QueryPerformanceCounter(&t);
    else {
        GetSystemTimeAsFileTime(&f);
        t.QuadPart = f.dwHighDateTime;
        t.QuadPart <<= 32;
        t.QuadPart |= f.dwLowDateTime;
    }

    t.QuadPart -= offset.QuadPart;
    microseconds = (double)t.QuadPart / frequencyToMicroseconds;
    t.QuadPart = microseconds;
    tv->tv_sec = t.QuadPart / 1000000;
    tv->tv_usec = t.QuadPart % 1000000;
    return (0);
}