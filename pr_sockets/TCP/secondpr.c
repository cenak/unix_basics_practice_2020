#include<io.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <winsock2.h>
#include <winsock.h>

#define ECHOMAX 255 /* Longest string to echo */

#pragma comment(lib,"ws2_32.lib") //Winsock Library

#define BUFLEN 8182	//Max length of buffer
#define PORT 8888	//The port on which to listen for incoming data


void HandleClient(int sock);

int main(int argc , char *argv[])
{
	WSADATA wsa;
	SOCKET s , new_socket;
	struct sockaddr_in server , client;
	int clientAddrSize;
	char *message;
	int recv_size;

	printf("\nInitialising Winsock... ");
	if(WSAStartup(MAKEWORD(2,2),&wsa) != 0)
	{
		printf("Failed. Error Code : %d",WSAGetLastError());
		return 1;
	}
	
	printf("Initialised.\n");
	
	//Create a socket
	if((s = socket(AF_INET , SOCK_STREAM , 0 )) == INVALID_SOCKET)
	{
		printf("Could not create socket : %d" , WSAGetLastError());
	}

	printf("Socket created.\n");
	
	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(PORT);
	
	//Bind
	if(bind(s ,(struct sockaddr *)&server , sizeof(server)) == SOCKET_ERROR)
	{
		printf("Bind failed with error code : %d" , WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	
	printf("Bind done");

	//Listen to incoming connections
	listen(s , 3);
	
	//Accept and incoming connection
	printf("Waiting for incoming connections...");
	
	clientAddrSize = sizeof(struct sockaddr_in);
	
	while (1)
	{
		// Ожидаем подключений от клиентов
		if ((new_socket = accept(s, (struct sockaddr *) &client, &clientAddrSize)) < 0)
		{
			printf("Failed to accept client connection");
			break;
		}
		printf("Client connected\n");
		HandleClient(new_socket);
	}

	closesocket(s);
	WSACleanup();
	
	return 0;
}

void HandleClient(int sock)
{
	char buffer[BUFLEN];
	int received = -1;
	if ((received = recv(sock, buffer, BUFLEN, 0)) < 0) // Принять сообщение
	{
		printf("Failed to receive initial bytes from client");
		close(sock);
		return;
	}
	
	// Отправить данные и посмотреть нет ли больше данных для приема
	while (received > 0)
	{
		// Отослать то, что приняли
		if (send(sock, buffer, received, 0) != received)
		{
			printf("Failed to send bytes to client");
			close(sock);
			return;
		}
		// Есть ли еще данные?
		if ((received = recv(sock, buffer, BUFLEN, 0)) < 0)
		{
			printf("Failed to receive additional bytes from client");
			close(sock);
			return;
		}
	}
	close(sock);
}