#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int pid;
	int res;
	if(pid = fork())
	{
		printf("I am parent process\n");
		printf("Child pid = %d\n", pid);
		wait(&pid);
	}
	else
	{
		printf("I am child process\n");
		printf("My pid = %d\n", getpid());
	}
	
	return 0;
}
