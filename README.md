Выполненные практические задания по дисциплине "Основы Unix"

* [x]  Fork practice (#1)
* [x]  Shared memory (#2)
* [x]  Print "Hello" by UDP command (#3)
* [x]  Creating symbol driver and kernel module (#4)
* [ ]  Core isolating (#5)
* [x]  Socket practice (#6)
* [x]  Memory segments and cache (#7)
* [x]  Bash script (#8)